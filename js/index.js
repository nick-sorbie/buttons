require([
    "js/underscore.min.js", 
    "js/utils.js",
    "js/game.js",
    "js/level.js",
    "js/gameChangers.js",
    "js/button.js"
], function() {
    const currentLevel = getCurrentLevel();
    generateAllObjects();
    size = calculateSize(currentLevel);
    const level = getLevel(currentLevel);
    const buttons = level.times;
    updateCounter(0);
    const {height, width} = getHeightAndWidth();
    const buttonsHtml = [];
    game.innersHtml = [];
    game.levelButtonsArray = _.range(1, currentLevel + 1);
    generateChunks();
    buttons.forEach(function(value, key){
        buttonsHtml.push(loadButtons(value, key));
    });
    game.gameDom = new Vue({
        el: '#game-body',
        data: {
            buttons: buttonsHtml,
            inners: game.innersHtml
        }
    });
    document.getElementById("game-body").style.height = height;
    document.getElementById("game-body").style.width = width;
});
