function returnSpecificWeaponFunction(weapon) {

    const weaponFunction = function(buttonObject) {
        const button = event.currentTarget;
        const currentButtonNumber = parseInt(button.dataset.button,10);
        let directions = [];
        switch (weapon) {
            case 'slowDown' :
                changeSpeeds(weapon);
                mainClassChange('outer_main',
                    `heat${game.timerSpeed - game.changeAmount}`,
                    `heat${game.timerSpeed}`);
                return;
                break;
            case 'speedUp' :
                changeSpeeds(weapon);
                mainClassChange('outer_main',
                    `heat${game.timerSpeed + game.changeAmount}`,
                    `heat${game.timerSpeed}`);
                return;
            case 'pause' :
                return pauseTimers();
            case 'win' :
                return allSelected();
            }
        const neighbourSelection = buttonObject.neighbours;
        if(neighbourSelection.length > 0) {
            for (var i = 0; (i < neighbourSelection.length); i++) {
                const value = neighbourSelection[i];
                const currentNeighbour = game.gameDom.$el.children[value - 1];
                const currentNeighbourObject = game.gameDom.buttons[value - 1];

                if (game.changeHistory.indexOf(currentNeighbourObject.button) !== -1) {break;}
                activateButton(currentNeighbourObject, currentNeighbour);

                if (i > 0 && currentNeighbourObject.weapon !== '') {
                    weaponFunction(currentNeighbourObject);
                    if (currentNeighbourObject.weapon !== 'good_square') {
                        return false;
                    }
                }
                game.changeHistory.push(currentNeighbourObject.button);
            }
            return false;
        }
        return false;
    }
    return weaponFunction;
}

function getDirections (weapon) {
    switch (weapon) {
        case 'good_up' :
        return [2];
    case 'good_down' :
        return [8];
    case 'good_left' : 
        return [4];
    case 'good_right' :
        return [6];
    case 'good_square' :
        return [1,2,3,4,5,6,7,8,9];
    }
    return [];
}

function changeSpeeds(weapon){
    switch (weapon) {
        case 'slowDown' :
            game.gameDom.$children.forEach(function(value, key){
                game.gameDom.$children[key].$el.attributes[2].nodeValue = game.changeAmount +
                parseInt(game.gameDom.$children[key].$el.attributes[2].nodeValue, 10);
            });
            return;
        case 'speedUp' :
            game.gameDom.$children.forEach(function(value, key){
                game.gameDom.$children[key].$el.attributes[2].nodeValue =
                parseInt(game.gameDom.$children[key].$el.attributes[2].nodeValue, 10) -
                game.changeAmount;
            });
        return;
        }
}

function mainClassChange(outerId, remove = null, add = null){
    if (remove) {
        document.getElementById(outerId).classList.remove(remove);
    }
    if (add) {
        document.getElementById(outerId).classList.add(add);
    }
}

function pauseTimers() {
    if (!game.isPaused) {
        game.isPaused = true;
        game.noTimers.forEach(function(value, key) { 
            const pausedButton = game.gameDom.$children[value.match(/\d+/)[0] - 1].$el;
            if(game.timers[`timer_${value}`]) {
                game.timers[`timer_${value}`].pause();
                pausedButton.className += " paused";
                setTimeout(function() {
                    game.isPaused = false;
                    pausedButton.classList.remove('paused');
                    if(game.timers[`timer_${value}`]) {
                        game.timers[`timer_${value}`].start();
                    }
                }, game.pauseLength);
            }
        });
    }
}

function getneighbours(thisButtonNumber, directions) {
    const currentRowIndex = Math.ceil(thisButtonNumber/game.noCols),
    currentRow = game.levelButtonsArrayGroups[currentRowIndex], 
    rowButtonPosition = currentRow.indexOf(thisButtonNumber),
    neightbours = [];

    if (directions.length > 1) {
        //Square
        neightbours.push(thisButtonNumber);
        if (currentRowIndex > 1) {
            const previousRow = game.levelButtonsArrayGroups[currentRowIndex - 1];
            //Top
            neightbours.push(previousRow[rowButtonPosition]);
            //Top Left
            if (rowButtonPosition > 0) {
                neightbours.push(previousRow[rowButtonPosition - 1]);
            }
            //Top Right
            if (rowButtonPosition < (game.noCols -1)) {
                neightbours.push(previousRow[rowButtonPosition + 1]);
            }
        }
        if (currentRowIndex < game.noRows) {
            const nextRow = game.levelButtonsArrayGroups[currentRowIndex + 1];
            //Bottom
            neightbours.push(nextRow[rowButtonPosition]);
            //Bottom Left
            if (rowButtonPosition > 0) {
                neightbours.push(nextRow[rowButtonPosition - 1]);
            }
            //Bottom Right
            if (rowButtonPosition < (game.noCols -1)) {
                neightbours.push(nextRow[rowButtonPosition + 1]);
            }
        }
        //Left
        if (rowButtonPosition > 0) {
            neightbours.push(currentRow[rowButtonPosition - 1]);
        }
        //Right
        if (rowButtonPosition < (game.noCols -1)) {
            neightbours.push(currentRow[rowButtonPosition + 1]);
        }

    } else if(directions.length === 1) {
        switch (directions[0]) {
            case 2 : {
                if (currentRowIndex <= 1) return [];
                neightbours.push(thisButtonNumber);
                for(let i = (currentRowIndex - 1); i > 0; i--)
                {
                    const previousRow = game.levelButtonsArrayGroups[i];
                    neightbours.push(previousRow[rowButtonPosition]);
                }
            }
            break;
            case 4 : {
                if (currentRowIndex < 1) return [];
                for(let i = rowButtonPosition; i >= 0; i--)
                {
                    neightbours.push(currentRow[i]);
                }
            }
            break;
            case 6 :{
                if (currentRowIndex > game.noCols) return [];
                for(let i = rowButtonPosition; i < game.noCols; i++)
                {
                    neightbours.push(currentRow[i]);
                }
            }
            break;
            case 8 : {
                if (currentRowIndex >= game.noRows) return [];
                neightbours.push(thisButtonNumber);
                for(let i = (currentRowIndex + 1); i <= game.noRows; i++)
                {
                    const nextRow = game.levelButtonsArrayGroups[i];
                    neightbours.push(nextRow[rowButtonPosition]);
                }
            }
            break;
        }
    }
    return neightbours;
}

function randomlyPlaceGameChangers() {
    if(Math.random() < game.gameChangersFrequency/100) {
        return selectRandomGameChanger();
    }
    return false;
}

function generateAllObjects() {
    game.level.complete = false;
    addObjects(game.gameChangerObjects);
}

function addObjects(gameChangingObjects) {
    for (const key in gameChangingObjects) {
        // skip loop if the property is from prototype
        if (!gameChangingObjects.hasOwnProperty(key)) continue;
        const obj = gameChangingObjects[key];
        for(let i = 0; i < obj.frequency; i++)
        {
            game.allObjects.push(key);
        }
    }
}

function selectRandomGameChanger() {
    return game.allObjects[Math.floor(Math.random()*game.allObjects.length)];
}