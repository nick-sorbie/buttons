let size = null;

const loadButtons = function(timer, key) {
    const buttonNo = key + 1;
    const buttonId = `button_${buttonNo}_id`;
    createButton(size, buttonId, buttonNo);
    let weapon = '';
    weapon = randomlyPlaceGameChangers();
    const directions = getDirections(weapon);
    return {
        id : `${buttonId}_outer`,
        onClick : bindClickListeners(weapon),
        classes : `button_outer ` +
        `game_changer_${weapon} ` + 
        `neighbour_number_${buttonNo}`,
        button : buttonNo,
        weapon : (weapon) ? weapon : '',
        neighbours : getneighbours(buttonNo, directions),
        timer : timer,
        progress : 0
      };
}
function createButton(size, buttonId, buttonNo) {
    const buttonHtml = getButtonHtml(size, buttonId, buttonNo);
    return buttonHtml;
}
function randId() {
     return Math.random().toString(36).substr(2, 10);
}
function getButtonHtml(size, buttonId, buttonNo) {
    game.innersHtml.push({
        template: `<div id='${buttonId}' 
        class='radial_progress_${size} 
        button_shape button_${size} 
        neighbour_number_${buttonNo}'/>` 
    });
}

function updatePercent(uniqueButtonId, timer, uniqueButtonChildId) {
    game.intervals[`interval_${uniqueButtonId}`] = setInterval(function(){
        game.gameDom.$children[uniqueButtonChildId].$el.setAttribute('data-progress',
            getPercent(uniqueButtonId, timer));
    }, 10);
}

function getPercent(uniqueButtonId, origionalTime) {
    const remaining = game.timers[`timer_${uniqueButtonId}`] ? game.timers[`timer_${uniqueButtonId}`].getTimeLeft() : false;
    let percenatage = 0;
    if (remaining) {
        percenatage = remaining/origionalTime * 100;
    }
    return parseInt(percenatage);
}

function bindClickListeners(weapon) {
    if (weapon.length > 0) {
        const weaponFunction = returnSpecificWeaponFunction(weapon);
        return function(){
            game.changeDirection = false;
            game.changeHistory = [];
            weaponFunction(this);
        };
    } else {
        return function(){
            game.changeHistory = [];
            activateButton(this);
        };
    }
}

function innerCircle(weapon){
    return `<div class="circle">` +
                `<div class="mask full">` +
                    `<div class="fill"></div>` +
                `</div>` +
                `<div class="inset">` +
                `<i class="${weapon ? 'fa ' +
                game.gameChangerObjects[weapon].icon :false}" aria-hidden="true"></i>` + 
                `</div>` +
            `</div>`
}

const activateButton = function(buttonObject, buttonTarget = false) {
    const button = buttonTarget || event.currentTarget,
    uniqueButtonId = `button_${buttonObject.button}_id`,
    uniqueButtonChildId = buttonObject.button - 1;

    if (button && !button.classList.contains('button_active')) {

        const innerButton = game.gameDom.$children[uniqueButtonChildId].$el;
        innerButton.innerHTML = innerCircle(buttonObject.weapon);

        const countdown = parseInt(innerButton.getAttribute('data-timer'), 10);

        updatePercent(uniqueButtonId, countdown, uniqueButtonChildId);
        game.noTimers.push(uniqueButtonId);

        button.classList.add('button_active');
        var activeButtons = document.getElementsByClassName("button_active");
        updateCounter(activeButtons.length);

        game.timers[`timer_${uniqueButtonId}`] = new timer(function() {
            clearInterval(game.intervals[`interval_${uniqueButtonId}`]);
            innerButton.setAttribute('data-progress', 0);

            setTimeout(function(){
                if (!game.level.complete) {
                    button.classList.remove('button_active');
                    changePropertyByClass(button, 'inset', 'backgroundColor', 'rgba(255, 255, 255, 0)');
                    delete game.timers[`timer_${uniqueButtonId}`];
                    game.noTimers.splice(game.noTimers.indexOf(uniqueButtonId), 1);
                    updateCounter(activeButtons.length);
                }
            }, game.timerSpeed);

        }, countdown);
    }
}

function changePropertyByClass(button, className, property, value){
    const selectedProperty = button.getElementsByClassName(className);
    if (selectedProperty[0]) {
        selectedProperty[0].style[property] = value;
    }
}

function updateCounter(selected){
    game.counterText = `${selected}/${game.currentLevel}`;
    document.getElementById(`counter`).innerHTML = game.counterText;
    if(selected === game.currentLevel) {
        allSelected();
    }
}

function timer(callback, delay) {
    var id, started, remaining = delay, running
    this.start = function() {
        running = true
        started = new Date()
        id = setTimeout(callback, remaining)
    }
    this.pause = function() {
        running = false
        clearTimeout(id)
        remaining -= new Date() - started
    }
    this.getTimeLeft = function() {
        if (running) {
            this.pause()
            this.start()
        }
        return remaining
    }
    this.getStateRunning = function() {
        return running
    }
    this.start()
}

function allSelected() {
    document.getElementById(`overlay`).classList.add(['show', 'win']);
    game.level.complete = true;
    activeButtons = document.getElementsByClassName("button_outer");
    for(var i = 0; i < game.currentLevel; i++)
    {
        activeButtons[i].classList.add("button_party");
        document.getElementById(`button_${i + 1}_id`).classList.remove('paused');
        if (i == game.currentLevel - 1) {
            document.getElementById(`overlay`).classList.remove(['show', 'win']);
        }
    }
}

