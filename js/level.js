const getLevel = (currentLevel) => {
    if (currentLevel <= game.maxLevels) {
        const rangeArray = range(levelToMillisecond(1),
            levelToMillisecond(currentLevel), game.gapLenght);
        return {
            'level' : currentLevel,
            'times' : shuffle(rangeArray),
        };
    } else {
        // party time
        const tempFix = currentLevel - game.maxLevels;
        const rangeArray = range(levelToMillisecond(1),
            levelToMillisecond(tempFix), game.gapLenght);
        return {
            'level' : tempFix,
            'times' : shuffle(rangeArray),
        };
    }
}

function levelToMillisecond(number) {
    return number * game.gapLenght;
}

function range(start, end, step) {
  const len = Math.floor((end - start) / step) + 1
  return Array(len).fill().map((_, idx) => start + (idx * step))
}
const result = range(9, 18, 0.83);

function generateAcceptableNumbers() {
    const acceptableNumbers = [1,2,4];
    for (i = 3; i < Math.sqrt(game.maxLevels); i++) {
        acceptableNumbers.push(i * (i - 1));
        acceptableNumbers.push(i * i);
    }
    return acceptableNumbers;
}

function getHeightAndWidth() {
    let height, width;
    const sqroot = Math.sqrt(game.currentLevel);
    const buttonSize = calculateSize(game.currentLevel, false) + 8; 
    //3 each side for margin + 1 each side border;

    if (sqroot % 1 === 0) {
        height = width = sqroot * buttonSize;
        game.noCols = game.noRows = sqroot;
    } else {
        game.noRows = Math.ceil(sqroot);
        game.noCols = Math.floor(sqroot);
        width = game.noCols * buttonSize;
        height = game.noRows * buttonSize;
    }
    game.noImportantNeighbours = (2 * game.noCols) - 1;

    return {
        height: `${height}px`,
        width: `${width}px`
    };
}

function generateChunks() {
    let i;
    let chunk = 1;
    for (i = 0; i < game.levelButtonsArray.length; i += game.noCols) {
        game.levelButtonsArrayGroups[chunk] = game.levelButtonsArray.slice(i,i+game.noCols);
        chunk++;
    }
}

const acceptableNumbers = generateAcceptableNumbers();

const getCurrentLevel = function() {
    if (!game.currentLevel) {
        game.currentLevel = acceptableNumbers[Math.floor(Math.random() * acceptableNumbers.length)];
    }
    return game.currentLevel;
}

const calculateSize = function(currentLevel, getClass = true) {
    //Keep consisten with sizes.less - automate?
    //returns default class size getclass to false to return the no of pixels
    const jumboMax = 6, largeMax = 20, mediumMax = 50, smallMax = 100, tinyMax = 200;
    if (currentLevel <= jumboMax) {
        return getClass ? 'jumbo' : 200;
    } else if(currentLevel <= largeMax) {
        return getClass ? 'large' : 150;
    } else if(currentLevel <= mediumMax) {
        return getClass ? 'medium' : 100;
    } else if(currentLevel <= smallMax) {
        return getClass ? 'small' : 75;
    } else return getClass ? 'tiny' : 50;
}

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;
  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}