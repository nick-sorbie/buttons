const game = {
    'currentLevel' : null,
    'gapLenght' : 400,
    'maxLevels' : 200,
    'pauseLength' : 7000,
    'isPaused' : false,
    'timers' : {},
    'intervals' : {},
    'noTimers' : [],
    'timerSpeed': 1000,
    'changeAmount': 1000,
    'minLevel' : 100,
    'maxLevel' : 2000,
    'changeDirection' : false,
    'changeHistory' : [],
    'levelButtonsArray' : [],
    'levelButtonsArrayGroups' : {},
    'currentLevelRowGroups' : null,
    'noCols' : null,
    'noImportantNeighbours' : null,
    'allObjects' : [],
    'gameChangersFrequency' : 75,
    'gameChangerObjects' : {
        // 'slowDown' : {
        //     'frequency' : 5,
        //     'icon' : 'fa-snowflake-o'
        // },
        // 'win' : {
        //     'frequency' : 0.00001,
        //     'icon' : 'fa-bolt'
        // },
        'good_up' : {
            'frequency' : 100,
            'icon' : 'fa-arrow-up'
        },
        'good_down' : {
            'frequency' : 100,
            'icon' : 'fa-arrow-down'
        },
        'good_left' : {
            'frequency' : 100,
            'icon' : 'fa-arrow-left'
        },
        'good_right' : {
            'frequency' : 100,
            'icon' : 'fa-arrow-right'
        },
        'good_square' : {
            'frequency' : 10,
            'icon' : 'fa-arrows-alt'
        },
        // 'pause' : {
        //     'frequency' : 5,
        //     'icon' : 'fa-pause'
        // },
        // 'speedUp' : {
        //     'frequency' : 5,
        //     'icon' : 'fa-clock-o'
        // }
    },
    level : {
        'complete' : null
    }
};